﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsyncResolve
{
    class AsyncResolve: Form
    {
        TextBox txtAddress;
        ListBox lsbResult;
        // Tham chiếu đến một phương thức sẽ được gọi khi một hoạt động không đồng bộ tương ứng hoàn tất.  
        private AsyncCallback OnResolved;

        [Obsolete]
        public AsyncResolve()
        {

            Text = "DNS Address Resolver";
            Size = new Size(400, 380);
            OnResolved = new AsyncCallback(Resolved);


            Label label1 = new Label();
            label1.Parent = this;
            label1.Text = "Enter address to resolve:";
            label1.AutoSize = true;
            label1.Location = new Point(10, 10);

            txtAddress = new TextBox();
            txtAddress.Parent = this;
            txtAddress.Size = new Size(200, 2 * Font.Height);
            txtAddress.Location = new Point(10, 35);

            lsbResult = new ListBox();
            lsbResult.Parent = this;
            lsbResult.Location = new Point(10, 65);
            lsbResult.Size = new Size(350, 20 * Font.Height);

            Button checkit = new Button();
            checkit.Parent = this;
            checkit.Text = "Resolve";
            checkit.Location = new Point(235, 32);
            checkit.Size = new Size(7 * Font.Height, 2 * Font.Height);
            checkit.Click += new EventHandler(ButtonResolveOnClick);
        }

        [Obsolete]
        void ButtonResolveOnClick(object obj, EventArgs ea)
        {
            lsbResult.Items.Clear(); // xóa tất cả các mục khỏi listbox
            string addr = txtAddress.Text;
            Object state = new Object();
            /*
             * Bắt đầu 1 yêu cầu không đồng bộ để phân giải tên máy chủ DNS
             * hoặc địa chỉ IP thành 1 phiên bản địa chỉ IP và trả về IAsyncResult
             * tham chiếu đến yêu cầu không đồng bộ
             */
            Dns.BeginResolve(addr, OnResolved, state);
        }

        [Obsolete]
        private void Resolved(IAsyncResult ar) //IAsyncResult : đại diện cho trạng thái của một hoạt động không đồng bộ 
        {
            string buffer;
            //IPHostEntry: cung cấp một lớp vùng chứa cho thông tin địa chỉ máy chủ lưu trữ internet
            // Dns : cung cấp chức năng phân giải tên miền đơn giản
            // EndResolve: kết thúc một yêu cầu không đồng bộ đối với thông tin DNS
            // trả về một đối tượng IPHostEntry có chứa thông tin DNS về máy chủ
            IPHostEntry iphe = Dns.EndResolve(ar);

            buffer = "Host name: " + iphe.HostName; // Lấy tên Dns của máy chủ và trả về một chuỗi chứa tên máy chủ chính cho máy chủ
            lsbResult.Items.Add(buffer); // Add buffer vào listbox

            foreach (string alias in iphe.Aliases) // duyệt vòng foreach Lấy từng alias trong danh sách các alias được liên kết với máy chủ lưu trữ
            {
                buffer = "Alias: " + alias;
                lsbResult.Items.Add(buffer);
            }
            // Duyệt vòng foreach lấy từng addrs trong danh sách địa chỉ ip được liên kết với máy chủ lưu trữ
            foreach (IPAddress addrs in iphe.AddressList) //trả về một mảng kiểu IPAddress chứa địa chỉ IP
            {
                buffer = "Address: " + addrs.ToString(); // chuyển đổi địa chỉ internet thành ký hiệu chuẩn của nó
                                                         // trả về một chuỗi có chứa địa chỉ IP
                lsbResult.Items.Add(buffer);
            }
        }

        [Obsolete]
        public static void Main()
        {
            Application.Run(new AsyncResolve());
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // AsyncResolve
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "AsyncResolve";
            this.ResumeLayout(false);

        }
    }
}
