﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetResolveInfo
{
    class Program
    {
        [Obsolete]
        public static void Main(string[] argv)
        {
            // lấy tên máy chủ của máy tính
            // trả về một chuỗi chứa tên máy chủ DNS của máy tính
            string hostName = Dns.GetHostName();

            // phân giải tên máy chủ DNS hoặc địa chỉ IP thành một phiên bản IPHostEntry
            // trả về một phiên bản IPHostEntry có chứa thông tin địa chỉ về máy chủ được chỉ định trong hostName
            IPHostEntry iphe = Dns.Resolve(hostName);

            Console.WriteLine("Information for {0}", hostName);

            // Lấy tên DNS của máy chủ. ex: ChuThuong
            Console.WriteLine("Host name: {0}", iphe.HostName);

            // Duyệt vòng for lấy alias trong danh sách Aliases được liên kết với máy chủ lưu trữ
            foreach (string alias in iphe.Aliases)
            {
                Console.WriteLine("Alias: {0}", alias);
            }

            // Duyệt vòng for lấy địa chỉ IP trong danh sách địa chỉ IP đươc liên kết với máy chủ lưu trữ
            foreach (IPAddress address in iphe.AddressList)
            {
                Console.WriteLine("Address: {0}", address.ToString());
            }

        }
    }
}
