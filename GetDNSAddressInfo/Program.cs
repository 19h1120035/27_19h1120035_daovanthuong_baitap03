﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetDNSAddressInfo
{
    class Program
    {
        [Obsolete]
        public static void Main(string[] argv)
        {
            try
            {
                // lấy tên máy chủ của máy tính
                // trả về một chuỗi chứa tên máy chủ DNS của máy tính
                string hostName = Dns.GetHostName();

                // Trả về một mảng kiểu IPAddress chứa địa chỉ IP cho máy chủ
                IPAddress[] addr = Dns.GetHostAddresses(hostName);
                IPAddress iPAddress = null;

                foreach (IPAddress item in addr)
                {
                    iPAddress = item;
                }

                // IPHostEntry: Cung cấp một lớp vùng chứa cho thông tin địa chỉ máy chủ
                // GetHostByAddress(IPAddress): Tạo một IPHostEntry từ IPAddress được chỉ định và trả về một IPHostEntry.
                IPHostEntry iphe = Dns.GetHostByAddress(iPAddress);

                Console.WriteLine("Information for {0}",iPAddress.ToString());

                // Lấy tên DNS của máy chủ. ex: ChuThuong
                Console.WriteLine("Host name: {0}", iphe.HostName);

                // Duyệt vòng for lấy alias trong danh sách Aliases được liên kết với máy chủ lưu trữ
                foreach (string alias in iphe.Aliases)
                {
                    Console.WriteLine("Alias: {0}", alias);
                }

                // Duyệt vòng for lấy địa chỉ IP trong danh sách địa chỉ IP đươc liên kết với máy chủ lưu trữ
                foreach (IPAddress address in iphe.AddressList)
                {
                    Console.WriteLine("Address: {0}", address.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
