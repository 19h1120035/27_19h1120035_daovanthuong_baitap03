﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindDNSServers
{
    class Program
    {
        public static void Main()
        {
            try
            {
                while (true)
                {
                    Console.OutputEncoding = Encoding.UTF8;
                    // Cung cấp các đối tượng RegistryKey đại diện cho các khóa gốc trong Windows Registry
                    RegistryKey start = Registry.LocalMachine;// Chứa dữ liệu cấu hình cho máy cục bộ (HKEY_LOCAL_MACHINE)
                    string DNSservers = @"SYSTEM\CurrentControlSet\Services\Tcpip\Parameters";

                    // Truy xuất subKey dưới dạng Read-Only và trả về subKey được yêu cầu hoặc null nếu thao tác không thành công.
                    RegistryKey DNSserverKey = start.OpenSubKey(DNSservers);

                    if (DNSserverKey == null)
                    {
                        Console.WriteLine("Unable to open DNS servers key");
                        return;
                    }
                    Console.WriteLine("Nhập vào tên cần tìm: ");
                    string valueName = Console.ReadLine();

                    /*
                     * truy xuất giá trị được liên kết với tên được chỉ định. 
                     * trả về null nếu cặp tên / giá trị không tồn tại trong sổ đăng ký.
                     */

                    /*
                     * ex: valueName: HostName -> serverlist: ChuThuong
                     * ex: valueName: Ahihi -> serverlist: Hi guys , I'm Chu Thuong
                     * duyệt mảng servers: 
                     * -> server[0]: Hi
                     * -> server[1]: guys
                     * -> server[2]: ,
                     * -> server[3]: I'm
                     * -> server[4]: Chu
                     * -> server[5]: Thuong
                     */
                    string serverlist = (string)DNSserverKey.GetValue(valueName);

                    Console.WriteLine("DNS Servers: {0}", serverlist);
                    DNSserverKey.Close();
                    start.Close();

                    char[] token = new char[1]; // Tạo 1 mảng có 1 phần tử là kí tự khoảng trắng
                    token[0] = ' ';
                    /* 
                     tạo 1 mảng servers gồm các từ được cắt bởi kí tự khoảng trắng
                     ex: serverlist = "Chu Thương 1363";
                     servers[0] = "Chu";
                     servers[1] = "Thương";
                     servers[2] = "1363";
                     */
                    string[] servers = serverlist.Split(token);

                    foreach (string server in servers) // duyệt mảng servers
                    {
                        Console.WriteLine("DNS server: {0}", server); // lấy từng giá trị trong mảng
                        // ex: DNS server: Chu
                        //     DNS server: Thuong
                        //     DNS server: 1363
                    }
                    Console.WriteLine("===========================");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            //Console.OutputEncoding = Encoding.UTF8;
            //Console.WriteLine("Nhập vào value: ");
            //string strss = Console.ReadLine();
            //RegistryWrite(strss);

            //Console.WriteLine("Server: "+ RegistryRead());
        }
        //public static void RegistryWrite(string str)
        //{

        //    RegistryKey registryKey = Registry.CurrentUser;
        //    string DNSservers = @"AdvanceEngineering";

        //    RegistryKey DNSserverKey = registryKey.CreateSubKey(DNSservers);
        //    DNSserverKey.SetValue("NameServer", str);
        //}
        //public static string RegistryRead()
        //{
        //    RegistryKey registryKey = Registry.CurrentUser;
        //    string DNSservers = @"AdvanceEngineering";

        //    RegistryKey DNSserverKey = registryKey.OpenSubKey(DNSservers);
        //    return DNSserverKey.GetValue("NameServer").ToString();
        //}
    }
}
