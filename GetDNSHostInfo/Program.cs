﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetDNSHostInfo
{
    class Program
    {
        [Obsolete]
        public static void Main(string[] argv)
        {
            // lấy tên máy chủ của máy tính
            // trả về một chuỗi chứa tên máy chủ DNS của máy tính
            string hostName = Dns.GetHostName();

            // lấy thông tin DNS cho tên máy chủ DNS được chỉ định
            // trả về một đối tượng IPHostEntry có chứa thông tin máy chủ cho địa chỉ được chỉ định trong tên máy chủ
            IPHostEntry results = Dns.GetHostByName(hostName);

            // Lấy tên DNS của máy chủ. ex: ChuThuong
            Console.WriteLine("Host name: {0}", results.HostName);

            // Duyệt vòng for lấy alias trong danh sách Aliases được liên kết với máy chủ lưu trữ
            foreach (string alias in results.Aliases)
            {
                Console.WriteLine("Alias: {0}", alias);
            }

            // Duyệt vòng for lấy địa chỉ IP trong danh sách địa chỉ IP đươc liên kết với máy chủ lưu trữ
            foreach (IPAddress address in results.AddressList)
            {
                Console.WriteLine("Address: {0}", address.ToString());
            }
        }
    }
}
